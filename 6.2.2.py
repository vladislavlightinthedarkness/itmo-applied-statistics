from decimal import *
import argparse
import os

def output(name, n, p, pTrue, D, DTrue):
	print('_' * 50)
	print('Для выборки объема',str(n) ,':')
	print()
	print('     Оценка',name,':',str(Decimal(p).quantize(Decimal('0.001'))))
	print('     Погрешность оценки',name,': ',str(Decimal(abs(pTrue - p)).quantize(Decimal('0.001'))))
	print('     Оценка дисперсии:', str(Decimal(D).quantize(Decimal('0.001'))))
	print('     Погрешность оценки дисперсии:' , str(Decimal(abs(DTrue - D)).quantize(Decimal('0.001'))))

def bernoulli():
    try:
        p = input('Введите параметр p: ')
        p = float(p)
    except:
        print("Неверные параметры!")
        bernoulli*()
    output('p', 10, E1, p, D1, p * (1 - p))
    output('p', 10000, E4, p, D4, p * (1 - p))

def binomial():
    try:
        n = input('Введите параметр n: ')
        n = int(n)
        p = input('Введите параметр p: ')
        p = float(p)
    except:
        print("Неверные параметры!")
        binomial()
    output('p', 10, E1 / n, p, D1, n * p * (1 - p))
    output('p', 10000, E4 / n, p, D4, n * p * (1 - p))

def geometric():
    try:
        p = input('Введите параметр p: ')
        p = float(p)
    except:
        print("Неверные параметры!")
        geometric()
    output('p', 10, 1 / E1, p, D1, (1 - p) / p ** 2)
    output('p', 10000, 1 / E4, p, D4, (1 - p) / p ** 2)

def poisson():
    try:
        l = input('Введите параметр \u03BB: ')
        l = float(l)
    except:
        print("Неверные параметры!")
        poisson()
    output('\u03BB', 10, E1, l, D1, l)
    output('\u03BB', 10000, E4, l, D4, l)

def uniform():
    try:
        a = input('Введите параметр a: ')
        b = input('Введите параметр b: ')
        a = float(a)
        b = float(b)
    except:
        print("Неверные параметры!")
        uniform()
    output('a', 10, 2 * E1 - b, a, D1, (b - a) ** 2 / 12)
    output('a', 10000, 2 * E4 - b, a, D4, (b - a) ** 2 / 12)

def exponential():
    try:
        l = input('Введите параметр \u03BB: ')
        l = float(l)
    except:
        print("Неверный параметр!")
        exponential()
    output('\u03BB', 10, 1 / E1, l, D1, 1 / l ** 2)
    output('\u03BB', 10000, 1 / E4, l, D4, 1 / l ** 2)

def normal():
    try:
        a = input('Введите параметр a: ')
        d = input('Введите параметр \u03C3\u00B2: ')
        a = float(a)
        d = float(d)
    except:
        print("Неверные параметры!")
        normal()
    output('a', 10, E1, a, D1, d)
    output('a', 10000, E4, a, D4, d)


if __name__ == "__main__":
    typesfunc = ["bernoulli","binomial","geometric","poisson","uniform","exponential","normal"]
    parser = argparse.ArgumentParser(description="123")
    parser.add_argument('smallfile', type=str, help="Файл с выборкой объема 10")
    parser.add_argument('bigfile', type=str, help="Файл с выборкой объема 10000")
    parser.add_argument('typefunction', type=str, help="|".join(typesfunc))
    args = parser.parse_args()
    sample = [[], []]
    address = [f"{args.smallfile}.csv", f"{args.bigfile}.csv"]
    for i in range(2):	
	    for j in open(address[i]):
		    if j[-1] == '\n':
			    j = j[:-1]
		    sample[i].append(float(j))


    E1 = sum(sample[0]) / 10
    D1 = sum([(i - E1) ** 2 for i in sample[0]]) / 10
    E4 = sum(sample[1]) / 10000
    D4 = sum([(i - E4) ** 2 for i in sample[1]]) / 10000
    for typef in typesfunc:
        if typef in args.typefunction:
            globals()[typef]()
